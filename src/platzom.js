// lenguaje platzom

export default function platzom(str) {
  let translation = str;

  // si es palindrome ninguna regla anterior cuenta y 
  // se debe retornar la palabra intercalando mayusculas y minusculas
  const reverse = (str) => str.split('').reverse().join('');
  const minMay = (str) => {
      const length = str.length;
      let translation = '';
      let capitalize = true;
      for (let i =0; i < length; i++) {
          const car = str.charAt(i);
          translation += capitalize ? car.toUpperCase() : car.toLowerCase();
          capitalize = !capitalize;
      }
      return translation;
  }

  if ( str == reverse(str) ) {
      return minMay(str);
  }

  // eliminar los caracteres ar al final de las palabras si existen
  if ( str.toLowerCase().endsWith('ar') ) {
      translation = str.slice(0, -2);
  }

  // añadir "ps" al final si la palabra comienza con "Z";
  if ( str.toLowerCase().startsWith('z') ) {
      translation = translation.concat('pe');
  }

  // si la palabra traducida tiene 10 o más letas, colocar un '-' a la mitad
  const length = translation.length;
  if ( length >= 10 ) {
      const index = Math.round(length / 2);
      const p1 = translation.slice(0, index); //hasta la mitad
      const p2 = translation.slice(index); // desde la mitad hasta el final
      translation = p1.concat('-').concat(p2)
  }
  
  return translation;
}
