# Platzom

Platzom es un idioma inventado para el [curso de fundamentos de javascript](https://platzi.com/js) de [Platzi](https://platzi.com)


## Descripción del idioma

- Si es palindrome ninguna regla anterior cuenta y se debe retornar la palabra intercalando mayusculas y minusculas.
- Eliminar los caracteres ar al final de las palabras si existen.
- Añadir "ps" al final si la palabra comienza con "Z".
- Si la palabra traducida tiene 10 o más letas, colocar un '-' a la mitad.


## Instalación

```
npm install platzom
```


## Como se usa

```
import platzom from 'platzom'

console.log(platzom('programar'))        // program
console.log(platzom('Zorro'))            // Zorrope
console.log(platzom('Zarpar'))           // Zarppe
console.log(platzom('transformacion'))   // tansfo-rmacion
console.log(platzom('arepera'))          // ArEpErA
```


## Creditos

- Juan Carlos Mendoza R.


## Licencia

- [MIT](https://opensource.org/licenses/MIT)