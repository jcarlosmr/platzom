'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = platzom;
// lenguaje platzom

function platzom(str) {
    var translation = str;

    // si es palindrome ninguna regla anterior cuenta y 
    // se debe retornar la palabra intercalando mayusculas y minusculas
    var reverse = function reverse(str) {
        return str.split('').reverse().join('');
    };
    var minMay = function minMay(str) {
        var length = str.length;
        var translation = '';
        var capitalize = true;
        for (var i = 0; i < length; i++) {
            var car = str.charAt(i);
            translation += capitalize ? car.toUpperCase() : car.toLowerCase();
            capitalize = !capitalize;
        }
        return translation;
    };

    if (str == reverse(str)) {
        return minMay(str);
    }

    // eliminar los caracteres ar al final de las palabras si existen
    if (str.toLowerCase().endsWith('ar')) {
        translation = str.slice(0, -2);
    }

    // añadir "ps" al final si la palabra comienza con "Z";
    if (str.toLowerCase().startsWith('z')) {
        translation = translation.concat('pe');
    }

    // si la palabra traducida tiene 10 o más letas, colocar un '-' a la mitad
    var length = translation.length;
    if (length >= 10) {
        var index = Math.round(length / 2);
        var p1 = translation.slice(0, index); //hasta la mitad
        var p2 = translation.slice(index); // desde la mitad hasta el final
        translation = p1.concat('-').concat(p2);
    }

    return translation;
}